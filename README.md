# protodrew.website v3.0



## What it is

this is the source code for my [personal website](https://protodrew.website)
## Why I made this

I made this because most website templates I see around are big, bloated, and full of unnecessary automations and stock photos that look tacky. My goal here is to create a site that strikes a good balance between attractiveness and weight, with the ability to be easily modified to people's liking.



## Why you want it

This acts as an awesome, lightweight digital resume that can be easily morphed into a landing page for a project, a blog, or anything else. Its also a great tool to see the basics of html/css as I tried to include as many comments as possible to explain what everything is doing.



## How to use it

Simply download this as a zip, remove the readme, modify the html/css to fit you're preferred style/content, then host on any hosting platform of your choice (I have had a good experience with [Bluehost](https://bluehost.com) in the past, soon I am going to self-host once I get to a permanent residence, when that happens I will create instructions for that).



## Contribution

Don't submit a pull request or put any issues on this page, this is my personal page showing the work I did and exists as a jumping off point for others rather than a collaborative project, however please feel free to fork this with your own modifications and host it on your own git platform of choice.
